package Customer.repositories;

import Customer.aggregate.ID;
import Customer.aggregate.Token;
import Customer.events.CustomerCreated;
import Customer.events.CustomerTokenAdded;
import Customer.events.CustomerTokenRemoved;
import messaging.MessageQueue;

import java.util.*;

public class ReadCustomerModelRepository {

    private Map<ID, Set<Token>> tokens = new HashMap<>();


    public ReadCustomerModelRepository(MessageQueue eventQueue) {
        eventQueue.addHandler(CustomerCreated.class, e -> apply((CustomerCreated) e));
        eventQueue.addHandler(CustomerTokenAdded.class, e -> apply((CustomerTokenAdded) e));
        eventQueue.addHandler(CustomerTokenRemoved.class, e -> apply((CustomerTokenRemoved) e));
    }

    public Token getToken(ID id) {
        return new ArrayList<>(tokens.getOrDefault(id, new HashSet<>())).get(0);
    }

    public Set<Token> getTokens(ID id){
        return new HashSet<>(tokens.getOrDefault(id, new HashSet<>()));
    }

    public void apply(CustomerCreated event) { //Stay empty
    }

    public void apply(CustomerTokenAdded event) {
        Set<Token> token = tokens.getOrDefault(event.getID(), new HashSet<>());
        token.add(event.getToken());
        tokens.put(event.getID(),token);
    }

    public void apply(CustomerTokenRemoved event) {
        Set<Token> token = tokens.getOrDefault(event.getID(), new HashSet<>());
        token.remove(event.getToken());
        tokens.put(event.getID(),token);
    }

}
