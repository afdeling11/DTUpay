
import Customer.aggregate.*;
import Customer.repositories.CustomerRepository;
import Customer.repositories.ReadCustomerModelRepository;
import Customer.services.CustomerService;
import Customer.services.SubCustomer;
import Customer.services.Transaction;
import messaging.Event;
import messaging.Message;
import messaging.MessageQueue;
import messaging.implementations.MessageQueueSync;
import org.junit.Assert;
import org.junit.Test;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CustomerUnitTest {
    Map<Integer, CompletableFuture<Event>> publishedEvents = new HashMap<>();
    int temp;

    MessageQueue queue = new MessageQueue() {
        @Override
        public void publish(Event message) {
            //var transactions = (Set<Transaction>) message.getArguments()[0];

            publishedEvents.get(temp).complete(message);
        }

        @Override
        public void publish(Message message) {

        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {

        }

        @Override
        public void addHandler(Class<? extends Message> event, Consumer<Message> handler) {

        }
    };
    CustomerRepository customerRepository;
    CustomerService customerService;
    Customer customer;
    Customer Expected;
    Set<Transaction> transaction;
    Transaction expectedTransaction;
    private CorrelationId correlationId;
    Map<Set<Transaction>, CorrelationId> correlationIds = new HashMap<>();
    Map<SubCustomer, CorrelationId> correlationIdMap = new HashMap<>();

    CompletableFuture<Set<Transaction>> registeredTransactions = new CompletableFuture<>();
    CompletableFuture<Customer> registeredCustomer = new CompletableFuture<>();

    private void setup_sync_queue() {
        MessageQueue eventQueue = new MessageQueueSync();
        customerRepository = new CustomerRepository(eventQueue);
        ReadCustomerModelRepository readRepository = new ReadCustomerModelRepository(eventQueue);
        customerService = new CustomerService(customerRepository, readRepository, queue);
    }

    public void create_new_customer(){
        var accountId = "1234";
        temp = Integer.parseInt(accountId);
        publishedEvents.put(temp, new CompletableFuture<>());
        new Thread(() -> {
            /*try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            customer = customerService.createCustomer(accountId);
            registeredCustomer.complete(customer);
        }).start();

        Event event = publishedEvents.get(temp).join();
        Assert.assertEquals(CustomerService.CUSTOMER_REGISTRATION_REQUESTED, event.getType());
        var w = event.getArgument(0, SubCustomer.class);
        correlationIdMap.put(w, event.getArgument(1, CorrelationId.class));

        SubCustomer c = new SubCustomer();
        c.setId(new ID(UUID.randomUUID()));
        c.setAccountID(accountId);
        customerService.updateAddToken(new Event(CustomerService.CUSTOMER_TOKENS_GENERATED, new Object[]{ c, correlationIdMap.get(w)}));

        Assert.assertNotNull(customer.getId());
    }

    public void applicationTest() {
        //customer = customerService.createCustomer("1234");
        //customer = customerRepository.getCustomerById(customer.getId());
        correlationId = CorrelationId.randomId();
        createTransactions();
        temp = transaction.iterator().next().getAmount();
        publishedEvents.put(temp, new CompletableFuture<>() );
        new Thread(()-> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            var result = customerService.requestTransactions();
            registeredTransactions.complete(result);
        }).start();

        Event event = publishedEvents.get(temp).join();
        Assert.assertEquals(CustomerService.CUSTOMER_TRANSACTIONS_REQUESTED, event.getType());
        var w = (Set<Transaction>) event.getArguments()[0];
        correlationIds.put(w,event.getArgument(1,CorrelationId.class));


        customerService.transactionReportGenerated(new Event(CustomerService.CUSTOMER_TRANSACTIONS_REPORT_GENERATED, new Object[]{ transaction, correlationIds.get(w)}));

        Assert.assertNotNull(registeredTransactions.join());
        Assert.assertEquals(2, registeredTransactions.join().size());

    }

    public void createTransactions() {
        var t1 = new Transaction();
        var t2 = new Transaction();

        var to1 = new Token();
        var to2 = new Token();

        t1.setId(new ID(UUID.randomUUID()));
        t1.setToken(to1);
        t1.setMerchantID("1234");
        t1.setAmount(10);
        t1.setDescription("test");

        t2.setId(new ID(UUID.randomUUID()));
        t2.setToken(to2);
        t2.setMerchantID("4321");
        t2.setAmount(20);
        t2.setDescription("Test 2");

        transaction = Stream.of(t1, t2).collect(Collectors.toSet());
    }

    @Test
    public void runTests() {
        setup_sync_queue();
        //create_new_customer();
        applicationTest();
    }
}
