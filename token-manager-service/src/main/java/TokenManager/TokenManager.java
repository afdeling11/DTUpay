package TokenManager;

import messaging.Event;
import messaging.MessageQueue;

import java.util.Set;

public class TokenManager {
    MessageQueue queue;

    public static final String CUSTOMER_REGISTRATION_REQUESTED = "CustomerRegistrationRequested";
    public static final String CUSTOMER_TOKENS_GENERATED = "CustomerTokensGenerated";
    public static final String CUSTOMER_REQUEST_NEW_TOKENS = "CustomerRequestNewTokens";

    public TokenManager(MessageQueue q) {
        queue = q;
        this.queue.addHandler(CUSTOMER_REGISTRATION_REQUESTED, this::handleTokenRequest);
        this.queue.addHandler(CUSTOMER_REQUEST_NEW_TOKENS, this::handleTokenRequest);
    }

    public Set<Token> requestTokens(Set<Token> tokens, int amount) throws IllegalArgumentException {
        if (amount > 5) { throw new IllegalArgumentException("Amount > 5"); }
        if (tokens.size() > 1) { throw new IllegalArgumentException("You already have > 1 token"); }

            for (int i = 0; i < amount;i++) {
            tokens.add(new Token());
        }
        return tokens;
    }

    public void handleTokenRequest(Event e) {
        Customer customer = e.getArgument(0, Customer.class);
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);
        Set<Token> tokens = requestTokens(customer.getTokens(),e.getArgument(2, Integer.class));
        customer.setTokens(tokens);
        queue.publish(new Event(CUSTOMER_TOKENS_GENERATED, new Object[] { customer, correlationId, tokens.size() }));
    }

}
