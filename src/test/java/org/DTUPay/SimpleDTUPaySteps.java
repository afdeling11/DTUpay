package org.DTUPay;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.CucumberOptions;
import org.DTUPay.businesslogic.legacy.Models.DPCustomer;
import org.DTUPay.businesslogic.legacy.Models.DTUPayException;
import org.DTUPay.businesslogic.legacy.Controllers.AccountController;
import org.DTUPay.businesslogic.BankService;
import org.DTUPay.businesslogic.legacy.Controllers.PaymentController;
import org.junit.platform.suite.api.ConfigurationParameter;

import java.math.BigDecimal;

import static org.junit.Assert.*;

@CucumberOptions(glue =  "")
@ConfigurationParameter(key = "GLUE_PROPERTY_NAME", value = "com.example.application")
public class SimpleDTUPaySteps {

    DPCustomer dpCustomer;
    DPCustomer merchant;
    BankService bank = BankService.getInstance();
    AccountController accountC = new AccountController();
    PaymentController paymentController = new PaymentController();
    Boolean successfulPayment = false;
    String id;

    @Given("a customer with a bank account with balance {int}")
    public void aCustomerWithABankAccountWithBalance(Integer balance) {
        try {
            id = bank.createBankAccountWithBalance("Peter","Pjerson","586123-9537",new BigDecimal(balance));
            dpCustomer = new DPCustomer(id);
        }catch (DTUPayException e){
            System.out.println(e.getMessage());
        }
    }

    @Given("that the customer is registered with DTU Pay")
    public void that_the_customer_is_registered_with_dtu_pay() {
        accountC.CreateAccount(dpCustomer.getId());
    }

    @Given("a merchant with a bank account with balance {int}")
    public void a_merchant_with_a_bank_account_with_balance(Integer balance) {
        try {
            String id = bank.createBankAccountWithBalance("Anders", "Ryanson","071280-6110", new BigDecimal(balance));
            merchant = new DPCustomer(id);
        }catch (DTUPayException e){
            System.out.println(e.getMessage());
        }
    }

    @Given("that the merchant is registered with DTU Pay")
    public void that_the_merchant_is_registered_with_dtu_pay() {
        accountC.CreateAccount(dpCustomer.getId());
    }

    @When("the merchant initiates a payment for {int} kr by the customer")
    public void the_merchant_initiates_a_payment_for_kr_by_the_customer(Integer amount) {
        successfulPayment = paymentController.pay(amount, dpCustomer.getId(), merchant.getId());
    }

    @Then("the payment is successful")
    public void the_payment_is_successful() {
        assertTrue(successfulPayment);
    }

    @Then("the balance of the customer at the bank is {int} kr")
    public void the_balance_of_the_customer_at_the_bank_is_kr(int int1){
        try {
            assertEquals(int1,  bank.getBalanceById(dpCustomer.getId()).intValue());
        } catch (DTUPayException e) {
            System.out.println(e.getMessage());
        }
    }

    @Then("the balance of the merchant at the bank is {int} kr")
    public void the_balance_of_the_merchant_at_the_bank_is_kr(int int1){
        try {
            assertEquals(int1,  bank.getBalanceById(merchant.getId()).intValue());
        } catch (DTUPayException e) {
            System.out.println(e.getMessage());
        }
    }

    @Then("the payment is unsuccessful")
    public void thePaymentIsUnsuccessful() {
        assertFalse(successfulPayment);
    }

    @Given("the customer {string} {string} with CPR {string} has a bank account")
    public void theCustomerWithCPRHasABankAccount(String firstName, String lastName, String cpr) {
        try {
            String id = bank.createBankAccount(firstName, lastName,cpr);
            dpCustomer = new DPCustomer(id);
        }catch (DTUPayException e){
            System.out.println(e.getMessage());
        }
    }

    @When("user gets registered in DTU-pay")
    public void userGetsRegisteredInDTUPay() {
        accountC.CreateAccount(dpCustomer.getId());
    }

    @Then("customer checks their balance which is {int} kr")
    public void customerChecksTheirBalanceWhichIsKr(int balance){
        try {
            assertEquals(balance, bank.getBalanceById(dpCustomer.getId()).intValue());
        } catch (DTUPayException e) {
            System.out.println(e.getMessage());
        }
    }

    @After
    public void retireAccount(){
        try {
            if (dpCustomer != null){
                bank.retireAccount(dpCustomer.getId());
            }

            if(merchant != null){
                bank.retireAccount(merchant.getId());
            }
        } catch (DTUPayException e) {
            System.out.println(e.getMessage());
        }
    }



}
