package org.DTUPay;

import messaging.MessageQueue;
import messaging.implementations.MessageQueueSync;
import org.DTUPay.businesslogic.AccountManager.aggregate.Customer;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;
import org.DTUPay.businesslogic.AccountManager.repositories.CustomerRepository;
import org.DTUPay.businesslogic.AccountManager.repositories.ReadCustomerModelRepository;
import org.DTUPay.businesslogic.AccountManager.services.CustomerService;
import org.DTUPay.businesslogic.legacy.Controllers.TokenManager.Token;
import org.DTUPay.businesslogic.TransactionManager.aggregate.Transaction;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ApplicationUnitTest {

    private CustomerService service;
    CustomerRepository repository;

    private void setup_sync_queues() throws InterruptedException, ExecutionException, Exception {
        MessageQueue eventQueue = new MessageQueueSync();
        repository = new CustomerRepository(eventQueue);
        ReadCustomerModelRepository readRepository = new ReadCustomerModelRepository(eventQueue);
        service = new CustomerService(repository, readRepository);
    }

    public void create_new_customer(){
        ID userId = service.createCustomer("1234");
        Customer customer = repository.getCustomerById(userId);
        assertEquals("1234",customer.getAccountID());
        assertTrue(customer.getTokens().isEmpty());
        assertTrue(customer.getTransactions().isEmpty());
    }

    public void validTokensAndTransactions() throws InterruptedException {
        ID userId = service.createCustomer("1234");
        service.updateAddToken(userId, Stream.of(
                        new Token("123456789"),
                        new Token("987654321"),
                        new Token("111111111"))
                .collect(Collectors.toSet()));
        service.updateTransactions(userId, Stream.of(
                new Transaction(Map.of(new Token("123456789"), userId),"5678",500,"Test"))
                .collect(Collectors.toSet()));

        Customer customer = repository.getCustomerById(userId);

        assertEquals(3,customer.getTokens().size());
        assertEquals(1,customer.getTransactions().size());

        service.updateRemoveToken(userId, service.token(userId));
        Thread.sleep(1000);
        assertEquals(Stream.of(new Token("987654321"),new Token("111111111")).collect(Collectors.toSet()), service.tokens(userId));


        assertEquals(Stream.of(
                new Transaction(Map.of(new Token("123456789"), userId),"5678",500,"Test")).collect(Collectors.toSet()),
                service.transactions(userId));
    }

    @Test
    public void runTests() throws Exception {
        setup_sync_queues();
        create_new_customer();
        validTokensAndTransactions();
    }



}
