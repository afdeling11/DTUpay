package org.DTUPay.businesslogic;
import dtu.ws.fastmoney.*;
import org.DTUPay.businesslogic.legacy.Models.DTUPayException;

import java.math.BigDecimal;
import java.util.List;


public class BankService {

    private static BankService bankServiceInstance = null;
    private dtu.ws.fastmoney.BankService bank;

    private BankService(){
        bank = new BankServiceService().getBankServicePort();

    }

    public static BankService getInstance(){
        if (bankServiceInstance == null){
            bankServiceInstance = new BankService();
        }
        return bankServiceInstance;
    }

    public String createBankAccount(String first, String lastname,String cprnumber) throws DTUPayException {
        User user = new User();
        user.setFirstName(first);
        user.setLastName(lastname);
        user.setCprNumber(cprnumber);

        String id = "";

        try {
            id = bank.createAccountWithBalance(user, new BigDecimal(0));
        } catch (BankServiceException_Exception e) {
            throw new DTUPayException(e.getFaultInfo().getErrorMessage(), e);
        }
        return id;
    }

    public String createBankAccountWithBalance(String first, String lastname,String cprnumber, BigDecimal balance) throws DTUPayException {
        User user = new User();
        user.setFirstName(first);
        user.setLastName(lastname);
        user.setCprNumber(cprnumber);

        String id = "";

        try {
            id = bank.createAccountWithBalance(user, balance);
        } catch (BankServiceException_Exception e) {
            throw new DTUPayException(e.getFaultInfo().getErrorMessage(), e);
        }
        return id;
    }

    public void retireAccount(String accountid) throws DTUPayException {
        try {
            bank.retireAccount(accountid);
        } catch (BankServiceException_Exception e) {
            throw new DTUPayException(e.getFaultInfo().getErrorMessage(), e);
        }
    }

    public BigDecimal getBalanceById(String accountid) throws DTUPayException {
        try {
            return bank.getAccount(accountid).getBalance();
        } catch (BankServiceException_Exception e) {
            throw new DTUPayException(e.getFaultInfo().getErrorMessage(), e);
        }
    }

    public boolean transferMoneyFromTo(String customerId, String merchantId, int amount, String description) throws DTUPayException {
        try {
            bank.transferMoneyFromTo(customerId, merchantId, new BigDecimal(amount), description);
            return true;
        } catch (BankServiceException_Exception e) {
            throw new DTUPayException(e.getFaultInfo().getErrorMessage(), e);
        }

    }

    public List<AccountInfo> getAccounts(){
        return bank.getAccounts();
    }


}
