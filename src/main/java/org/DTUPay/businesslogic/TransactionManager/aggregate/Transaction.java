package org.DTUPay.businesslogic.TransactionManager.aggregate;

import lombok.Value;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;
import org.DTUPay.businesslogic.legacy.Controllers.TokenManager.Token;

import java.util.Map;

@Value
public class Transaction {
    private Map<Token, ID> token;
    private String merchantID;
    private int amount;
    private String description;

    /* Business Logic */

    /* Event Handler */
}
