package org.DTUPay.businesslogic.AccountManager.repositories;

import messaging.MessageQueue;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;
import org.DTUPay.businesslogic.AccountManager.events.*;
import org.DTUPay.businesslogic.legacy.Controllers.TokenManager.Token;
import org.DTUPay.businesslogic.TransactionManager.aggregate.Transaction;

import java.util.*;

public class ReadCustomerModelRepository {

    private Map<ID, Set<Transaction>> transactions = new HashMap<>();
    private Map<ID, Set<Token>> tokens = new HashMap<>();


    public ReadCustomerModelRepository(MessageQueue eventQueue) {
        eventQueue.addHandler(CustomerCreated.class, e -> apply((CustomerCreated) e));
        eventQueue.addHandler(CustomerTokenAdded.class, e -> apply((CustomerTokenAdded) e));
        eventQueue.addHandler(CustomerTokenRemoved.class, e -> apply((CustomerTokenRemoved) e));
        eventQueue.addHandler(CustomerTransactionAdded.class, e -> apply((CustomerTransactionAdded) e));
        //eventQueue.addHandler(CustomerTransactionRemoved.class, e -> apply((CustomerTransactionRemoved) e));
    }

    public Set<Transaction> getTransactions(ID id){
        return new HashSet<>(transactions.getOrDefault(id, new HashSet<>()));
    }

    public Token getToken(ID id) {
        return new ArrayList<>(tokens.getOrDefault(id, new HashSet<>())).get(0);
    }

    public Set<Token> getTokens(ID id){
        return new HashSet<>(tokens.getOrDefault(id, new HashSet<>()));
    }

    public void apply(CustomerCreated event) { //Stay empty
    }

    public void apply(CustomerTokenAdded event) {
        Set<Token> token = tokens.getOrDefault(event.getID(), new HashSet<>());
        token.add(new Token(event.getToken()));
        tokens.put(event.getID(),token);
    }

    public void apply(CustomerTokenRemoved event) {
        Set<Token> token = tokens.getOrDefault(event.getID(), new HashSet<>());
        token.remove(new Token(event.getToken()));
        tokens.put(event.getID(),token);
    }

    public void apply(CustomerTransactionAdded event) {
        Set<Transaction> transaction = transactions.getOrDefault(event.getID(), new HashSet<>());
        transaction.add(new Transaction(event.getToken(), event.getMerchantId(), event.getAmount(), event.getDescription()));
        transactions.put(event.getID(),transaction);
    }

    /*public void apply(CustomerTransactionRemoved event) {
        Set<Transaction> transaction = transactions.getOrDefault(event.getID(), new HashSet<>());
        transaction.remove(new Transaction(event.getToken(), event.getMerchantId(), event.getAmount(), event.getDescription()));
        transactions.put(event.getID(),transaction);
    }*/
}
