package org.DTUPay.businesslogic.AccountManager.repositories;

import messaging.MessageQueue;
import org.DTUPay.businesslogic.AccountManager.aggregate.Customer;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;

public class CustomerRepository {
    private EventStore eventStore;

    public CustomerRepository(MessageQueue messageQueue) {
        this.eventStore = new EventStore(messageQueue);
    }

    public Customer getCustomerById(ID id){
        return Customer.createFromEvents(eventStore.getEeventsFor(id));
    }

    public void save(Customer customer){
        eventStore.addEvents(customer.getId(),customer.getAppliedEvents());
        customer.clearAppliedEvents();
    }
}
