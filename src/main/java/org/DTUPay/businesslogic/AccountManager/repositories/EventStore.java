package org.DTUPay.businesslogic.AccountManager.repositories;

import lombok.NonNull;
import messaging.MessageQueue;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;
import org.DTUPay.businesslogic.AccountManager.events.Event;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class EventStore {

    private Map<ID, List<Event>> store = new ConcurrentHashMap<>();

    private MessageQueue eventBus;

    public EventStore(MessageQueue bus) {
        this.eventBus = bus;
    }

    public void addEvent(ID id, Event event) {
        if (!store.containsKey(event.getID())) {
            store.put(event.getID(), new ArrayList<>());
        }
        store.get(event.getID()).add(event);
        eventBus.publish(event);
    }

    public Stream<Event> getEeventsFor(ID id) {
        if (!store.containsKey(id)) {
            store.put(id, new ArrayList<>());
        }
        return store.get(id).stream();
    }

    public void addEvents(@NonNull ID id, List<Event> appliedEvents) {
        appliedEvents.stream().forEach(e -> addEvent(id, e));
    }
}
