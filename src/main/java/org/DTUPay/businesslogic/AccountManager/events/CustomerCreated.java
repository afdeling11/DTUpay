package org.DTUPay.businesslogic.AccountManager.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;

@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomerCreated extends Event {

    private static final long serialVersionUID =  -1599019626118724482L;
    ID customerID;
    String AccountID;


    @Override
    public ID getID() {
        return customerID;
    }
}
