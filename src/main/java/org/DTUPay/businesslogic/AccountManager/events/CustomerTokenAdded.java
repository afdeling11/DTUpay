package org.DTUPay.businesslogic.AccountManager.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;

@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomerTokenAdded extends Event{

    private static final long serialVersionUID = 3699730769270260597L;
    private ID id;
    private String token;


    @Override
    public ID getID() {
        return id;
    }
}
