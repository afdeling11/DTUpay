package org.DTUPay.businesslogic.AccountManager.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;

@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MerchantRefunded extends Event{

    @Override
    public ID getID() {
        return null;
    }
}
