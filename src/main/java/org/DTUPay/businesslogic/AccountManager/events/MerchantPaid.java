package org.DTUPay.businesslogic.AccountManager.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;

@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MerchantPaid extends Event{

    private static final long serialVersionUID =  -1599123626118724483L;
    ID merchantID;
    String customerAccountID;
    int amount;
    String description;

    @Override
    public ID getID() {
        return merchantID;
    }
}
