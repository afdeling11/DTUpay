package org.DTUPay.businesslogic.AccountManager.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;

@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomerTokenRemoved extends Event{

    private static final long serialVersionUID = 1596683920706802940L;
    private ID id;
    private String token;

    @Override
    public ID getID() {
        return id;
    }
}
