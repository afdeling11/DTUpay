package org.DTUPay.businesslogic.AccountManager.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;
import org.DTUPay.businesslogic.legacy.Controllers.TokenManager.Token;

import java.util.Map;

@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomerTransactionAdded extends Event{

    private static final long serialVersionUID = 6473986354743309952L;
    private ID id;
    private Map<Token, ID> token;
    private String merchantId;
    private int amount;
    private String description;


    @Override
    public ID getID() {
        return id;
    }
}
