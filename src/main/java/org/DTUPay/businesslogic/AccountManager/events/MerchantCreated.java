package org.DTUPay.businesslogic.AccountManager.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;

@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MerchantCreated extends Event{

    private static final long serialVersionUID =  -1599019626118724483L;
    ID merchantID;
    String AccountID;

    @Override
    public ID getID() {
        return merchantID;
    }
}
