package org.DTUPay.businesslogic.AccountManager.aggregate;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.DTUPay.businesslogic.TransactionManager.aggregate.Transaction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class Account {
    private ID id;
    private String accountID;
    private Set<Transaction> transactions = new HashSet<>();

    /* Business Logic */

    public static void registerAccount() {

    }

    public void deRegisterAccount() {

    }

    public void getReport() {

    }
}
