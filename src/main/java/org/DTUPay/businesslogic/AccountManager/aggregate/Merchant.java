package org.DTUPay.businesslogic.AccountManager.aggregate;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import messaging.Message;
import org.DTUPay.businesslogic.AccountManager.events.CustomerCreated;
import org.DTUPay.businesslogic.AccountManager.events.Event;
import org.DTUPay.businesslogic.AccountManager.events.MerchantCreated;
import org.DTUPay.businesslogic.BankService;
import org.DTUPay.businesslogic.TransactionManager.aggregate.Transaction;
import org.DTUPay.businesslogic.legacy.Models.DTUPayException;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Getter
public class Merchant extends Account {


    @Setter(AccessLevel.NONE)
    private List<Event> appliedEvents = new ArrayList<>();
    BankService bank = BankService.getInstance();

    private Map<Class<? extends Message>, Consumer<Message>> handlers = new HashMap<>();

    public static Merchant create(String accountId) {
        ID id = new ID(UUID.randomUUID());
        MerchantCreated event = new MerchantCreated(id, accountId);
        Merchant merchant = new Merchant();
        merchant.setId(id);
        merchant.setAccountID(accountId);
        merchant.appliedEvents.add(event);
        return merchant;
    }

    public static Merchant createFromEvents(Stream<Event> events){
        Merchant merchant = new Merchant();
        merchant.applyEvents(events);
        return merchant;
    }

    public Merchant() { registerEventHandlers(); }

    private void registerEventHandlers(){
        handlers.put(MerchantCreated.class, e -> apply((MerchantCreated) e));
    }



    /* Business Logic */

    public void Pay(String customerID,int amount, String description) {
        try {
            bank.transferMoneyFromTo(customerID,getAccountID(),amount,description);
        } catch (DTUPayException e) {
            System.out.println("Error handling... is lazy"); //todo
        }
        // add event for this
    }

    public void Refund() {
        // add event for this
    }




    /* Event Handling */
    private void applyEvents(Stream<Event> events) throws Error {
        events.forEachOrdered(this::applyEvent);
        if (this.getId() == null) {
            throw new Error("merchant does not exists");
        }
    }

    private void applyEvent(Event e) { handlers.getOrDefault(e.getClass(), this::missingHandler).accept(e); }

    private void missingHandler(Message e) { throw new Error("handler for "+e+" missing"); }

    private void apply(MerchantCreated event) {
        setId(event.getID());
        setAccountID(event.getAccountID());
    }

    public void clearAppliedEvents(){
        appliedEvents.clear();
    }
}
