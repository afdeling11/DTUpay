package org.DTUPay.businesslogic.AccountManager.aggregate;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import messaging.Message;
import org.DTUPay.businesslogic.AccountManager.events.*;
import org.DTUPay.businesslogic.legacy.Controllers.TokenManager.Token;
import org.DTUPay.businesslogic.TransactionManager.aggregate.Transaction;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public class Customer extends Account {
    private Set<Token> tokens = new HashSet<>();

    @Setter(AccessLevel.NONE)
    private List<Event> appliedEvents = new ArrayList<>();

    private Map<Class<? extends Message>, Consumer<Message>> handlers = new HashMap<>();

    public static Customer create(String accountID){
        ID ID = new ID(UUID.randomUUID());
        CustomerCreated event = new CustomerCreated(ID, accountID);
        Customer customer = new Customer();
        customer.setId(ID);
        customer.setAccountID(accountID);
        customer.appliedEvents.add(event);
        return customer;
    }

    public static Customer createFromEvents(Stream<Event> events) {
        Customer customer = new Customer();
        customer.applyEvents(events);
        return customer;
    }

    public Customer() { registerEventHandlers(); }

    private void registerEventHandlers(){
        handlers.put(CustomerCreated.class, e -> apply((CustomerCreated) e));
        handlers.put(CustomerTokenAdded.class, e -> apply((CustomerTokenAdded) e));
        handlers.put(CustomerTokenRemoved.class, e -> apply((CustomerTokenRemoved) e));
        handlers.put(CustomerTransactionAdded.class, e -> apply((CustomerTransactionAdded) e));
        //handlers.put(CustomerTransactionRemoved.class, e -> apply((CustomerTransactionRemoved) e));
        
    }

    /* Business Logic */

    private Token getToken(){
        return getTokens().stream().findFirst().get();
    }

    public void updateAddToken(Set<Token> tokens){
        addTokens(tokens);
        applyEvents(appliedEvents.stream());
    }

    public void updateRemoveToken(Set<Token> tokens){
        removeToken(tokens);
        applyEvents(appliedEvents.stream());
    }



    public void updateTransactions(Set<Transaction> transactions){
        addTransactions(transactions);
        applyEvents(appliedEvents.stream());
    }

    private void addTokens(Set<Token> tokens) {
        List<Event> events = tokens.stream()
                .map(token -> (Event)new CustomerTokenAdded(
                        getId(),
                        token.getToken()))
                .collect(Collectors.toList());
        appliedEvents.addAll(events);
        // add event for this
    }

    private void removeToken(Set<Token> tokens) {
        List<Event> events = tokens.stream().filter(tokens::contains)
                .map(token -> (Event)new CustomerTokenRemoved(
                        getId(),
                        token.getToken()))
                .collect(Collectors.toList());
        appliedEvents.addAll(events);
        // add event for this
    }

    private void addTransactions(Set<Transaction> transactions) {
        List<Event> events = transactions.stream()
                .map(transaction -> (Event)new CustomerTransactionAdded(
                        getId(),
                        Map.of(getToken(),getId()),
                        transaction.getMerchantID(),
                        transaction.getAmount(),
                        transaction.getDescription()))
                .collect(Collectors.toList());
        appliedEvents.addAll(events);
        // add event for this
    }

    /*private void removeTransactions(Set<Transaction> transactions) {
        List<Event> events = getTransactions().stream().filter(tr -> !transactions.contains(tr))
                .map(transaction -> (Event)new CustomerTransactionRemoved(
                        getId(),
                        Map.of(getToken(),getId()),
                        transaction.getMerchantID(),
                        transaction.getAmount(),
                        transaction.getDescription()))
                .collect(Collectors.toList());
        appliedEvents.addAll(events);
        // add event for this
    }*/

    /* Event Handling */

    private void applyEvents(Stream<Event> events) throws Error {
        events.forEachOrdered(this::applyEvent);
        if (this.getId() == null) {
            throw new Error("customer does not exists");
        }
    }

    private void applyEvent(Event e) { handlers.getOrDefault(e.getClass(), this::missingHandler).accept(e); }

    private void missingHandler(Message e) { throw new Error("handler for "+e+" missing"); }

    private void apply(CustomerCreated event) {
        setId(event.getID());
        setAccountID(event.getAccountID());
    }

    private void apply(CustomerTokenAdded event) {
        Token token = new Token(event.getToken());
        tokens.add(token);
    }
    private void apply(CustomerTokenRemoved event) {
        Token token = new Token(event.getToken());
        tokens.remove(token);

    }
    private void apply(CustomerTransactionAdded event) {
        Transaction transaction = new Transaction(event.getToken(),event.getMerchantId(),event.getAmount(),event.getDescription());
        getTransactions().add(transaction);
    }
    /*private void apply(CustomerTransactionRemoved event) {
        Transaction transaction = new Transaction(event.getToken(),event.getMerchantId(),event.getAmount(),event.getDescription());
        getTransactions().remove(transaction);
    }*/

    public void clearAppliedEvents(){
        appliedEvents.clear();
    }
}
