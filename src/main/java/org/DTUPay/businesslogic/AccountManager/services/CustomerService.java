package org.DTUPay.businesslogic.AccountManager.services;

import org.DTUPay.businesslogic.AccountManager.aggregate.Customer;
import org.DTUPay.businesslogic.AccountManager.aggregate.ID;
import org.DTUPay.businesslogic.AccountManager.repositories.CustomerRepository;
import org.DTUPay.businesslogic.AccountManager.repositories.ReadCustomerModelRepository;
import org.DTUPay.businesslogic.legacy.Controllers.TokenManager.Token;
import org.DTUPay.businesslogic.TransactionManager.aggregate.Transaction;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CustomerService {

    private CustomerRepository repository;
    private ReadCustomerModelRepository readCustomerModelRepository;

    public CustomerService(CustomerRepository repository, ReadCustomerModelRepository readCustomerModelRepository) {
        this.repository = repository;
        this.readCustomerModelRepository = readCustomerModelRepository;
    }

    /* Command operations */

    public ID createCustomer(String accountId){
        Customer customer = Customer.create(accountId);
        repository.save(customer);

        return customer.getId();

    }

    public void updateAddToken(ID id, Set<Token> tokens){
        Customer customer = repository.getCustomerById(id);
        customer.updateAddToken(tokens);

        repository.save(customer);
    }

    public void updateRemoveToken(ID id, Set<Token> tokens){
        Customer customer = repository.getCustomerById(id);
        customer.updateRemoveToken(tokens);

        repository.save(customer);

    }

    public void updateTransactions(ID id, Set<Transaction> transactions){
        Customer customer = repository.getCustomerById(id);
        customer.updateTransactions(transactions);

        repository.save(customer);
    }

    /* Query Operations */

    public Set<Transaction> transactions(ID id){
        return readCustomerModelRepository.getTransactions(id);
    }

    public Set<Token> token(ID id){
        return Stream.of(readCustomerModelRepository.getToken(id)).collect(Collectors.toSet());
    }

    public Set<Token> tokens(ID id){
        return readCustomerModelRepository.getTokens(id);
    }


}
