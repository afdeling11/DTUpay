package org.DTUPay.businesslogic.legacy.Services;

import org.DTUPay.businesslogic.legacy.Db.CustomerDb;
import org.DTUPay.businesslogic.legacy.Models.DPCustomer;

public class RegisterService {

    public boolean CustomerExists(String id){
        try {
            CustomerDb.getCustomerById(id);
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public DPCustomer createAccount(String accountId) throws NullPointerException{
        if(!CustomerExists(accountId) && !accountId.equals("")){
            DPCustomer DPCustomer = new DPCustomer(accountId);
            CustomerDb.createCustomer(DPCustomer);
            return DPCustomer;
        }
        throw new NullPointerException();
    }

    public DPCustomer getAccount(String accountId) throws NullPointerException {
        try {
            return CustomerDb.getCustomerById(accountId);
        } catch (NullPointerException e) {
            throw e;
        }
    }
}
