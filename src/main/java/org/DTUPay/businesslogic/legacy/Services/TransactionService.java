package org.DTUPay.businesslogic.legacy.Services;

import org.DTUPay.businesslogic.BankService;

import java.math.BigDecimal;

public class TransactionService {

    BankService bank = BankService.getInstance();

    public Boolean payment(int amount, String customerId, String merchantId){
        try{
            return bank.transferMoneyFromTo(customerId, merchantId, amount, "Random");

        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
}
