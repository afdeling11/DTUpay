package org.DTUPay.businesslogic.legacy.Models;

import lombok.Data;

@Data
public class DPCustomer {
    final String id;
}
