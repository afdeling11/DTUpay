package org.DTUPay.businesslogic.legacy.Models;

public class DTUPayException extends Exception {


    public DTUPayException(String msg, Throwable cause) {
        super(msg, cause);

    }
}
