package org.DTUPay.businesslogic.legacy.Models;

public class ServiceReturn {

    public Boolean ft;
    public String errorMessage;

    public ServiceReturn(Boolean ft, String errorMessage) {
        this.ft = ft;
        this.errorMessage = errorMessage;
    }
}
