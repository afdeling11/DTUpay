package org.DTUPay.businesslogic.legacy.Db;

import org.DTUPay.businesslogic.legacy.Models.DPCustomer;

import java.util.ArrayList;
import java.util.List;


public class CustomerDb {
    private static List<DPCustomer> DPCustomerDb = new ArrayList<>();

    public static DPCustomer getCustomerById(String id) throws NullPointerException {
        for(DPCustomer cust : DPCustomerDb){
            if (id.equals(cust.getId())){
                return cust;
            }
        }

        throw new NullPointerException();
    }

    public static void createCustomer(DPCustomer cust){
        DPCustomerDb.add(cust);
    }
}
