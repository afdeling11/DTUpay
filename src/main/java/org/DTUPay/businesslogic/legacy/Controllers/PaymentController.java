package org.DTUPay.businesslogic.legacy.Controllers;

import org.DTUPay.businesslogic.legacy.Services.TransactionService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/payment")
public class PaymentController {

    TransactionService transactionService = new TransactionService();

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Boolean pay(@QueryParam("amount") int amount, @QueryParam("cid") String cid, @QueryParam("mid") String mid) {
        return transactionService.payment(amount, cid, mid);
    }
}
