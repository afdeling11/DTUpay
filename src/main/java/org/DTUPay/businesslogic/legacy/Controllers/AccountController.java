package org.DTUPay.businesslogic.legacy.Controllers;

import org.DTUPay.businesslogic.legacy.Models.DPCustomer;
import org.DTUPay.businesslogic.legacy.Models.DTUPayException;
import org.DTUPay.businesslogic.BankService;
import org.DTUPay.businesslogic.legacy.Services.RegisterService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;

@Path("/account")
public class AccountController {

    RegisterService reg = new RegisterService();
    BankService bank = BankService.getInstance();

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    public String CreateAccount (String accountid){
        DPCustomer DPCustomer;
        System.out.println(accountid);
        try {
            DPCustomer = reg.createAccount(accountid);
            return DPCustomer.getId();
        } catch (NullPointerException e) {
            try {
                DPCustomer = reg.getAccount(accountid);
                return DPCustomer.getId();
            }catch (NullPointerException ex) {
                System.out.println(ex);
            }
        }
        return "";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String GetBankAccountID(@QueryParam("firstName") String firstName, @QueryParam("lastName") String lastName, @QueryParam("cprNumber") String cprNumber, @QueryParam("balance") int balance) {
        try {
            return bank.createBankAccountWithBalance(firstName, lastName, cprNumber, new BigDecimal(balance));
        }catch (DTUPayException e){
            System.out.println(e.getMessage());
        }
        return "";
    }

    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public void RetireBankAccount(@QueryParam("accountid") String accountID) {
        try {
            bank.retireAccount(accountID);
        } catch (DTUPayException e) {
            e.printStackTrace();
        }
    }
}
