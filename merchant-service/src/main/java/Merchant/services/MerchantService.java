package Merchant.services;

import Merchant.aggregate.ID;
import Merchant.aggregate.Merchant;
import Merchant.aggregate.MerchantTransaction;
import Merchant.repositories.MerchantRepository;
import Merchant.repositories.ReadMerchantModelRepository;
import Merchant.aggregate.CorrelationId;
import messaging.MessageQueue;
import messaging.Event;


import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class MerchantService {

    public static final String MERCHANT_TRANSACTION_INITIATED = "MerchantTransactionInitiated";
    public static final String MERCHANT_TRANSACTION_CREATED = "MerchantTransactionCreated";
    public static final String MERCHANT_TRANSACTION_REQUESTED = "MerchantTransactionRequested";
    public static final String MERCHANT_TRANSACTION_REPORT_GENERATED = "MerchantTransactionReportGenerated";

    private MerchantRepository repository;
    private ReadMerchantModelRepository readMerchantModelRepository;

    MessageQueue queue;
    //MessageQueue queue;
    private Map<CorrelationId, CompletableFuture<MerchantTransaction>> correlations = new ConcurrentHashMap<>();
    private Map<CorrelationId, CompletableFuture<Set<MerchantTransaction>>> correlationsTransaction = new ConcurrentHashMap<>();


    public MerchantService(MerchantRepository repository, ReadMerchantModelRepository readMerchantModelRepository, MessageQueue q){
        // (String eventType, Consumer<Event> handler)
        this.repository = repository;
        this.readMerchantModelRepository = readMerchantModelRepository;
        this.queue = q;
        this.queue.addHandler(MERCHANT_TRANSACTION_CREATED, this::handleMerchantTransaction);
        this.queue.addHandler(MERCHANT_TRANSACTION_REPORT_GENERATED, this::transactionReportGenerated);
    }

    /* Command operations */

    public ID createMerchant(String accountId) {
        Merchant merchant = Merchant.create(accountId);
        repository.save(merchant);
        return merchant.getId();
    }

    public void deregisterMerchant(ID id){
        repository.remove(id);
    }

    // initiate transaction

    public MerchantTransaction initiateTransaction(MerchantTransaction transaction) throws InterruptedException {
        CorrelationId correlationId = CorrelationId.randomId(); // creating an ID
        correlations.put(correlationId, new CompletableFuture<>()); // putting the id and a new completablefuture in the correlations map
        Event event = new Event(MERCHANT_TRANSACTION_INITIATED, new Object[] {transaction, correlationId}); // creating a new event
        queue.publish(event); // publishing the event to a queue
        MerchantTransaction t = correlations.get(correlationId).join();
        return t; // return a transaction based on the correlation ID and join the threads
    }

    public void handleMerchantTransaction(Event e){
        MerchantTransaction m = e.getArgument(0, MerchantTransaction.class); // get the transaction from the event
        CorrelationId cid = e.getArgument(1, CorrelationId.class); // get the correlationID from the event
        correlations.get(cid).complete(m); //  ???
    }

    // refund


    // see all transactions
    public Set<MerchantTransaction> requestTransactions(){
        var correlationId = CorrelationId.randomId();
        correlationsTransaction.put(correlationId, new CompletableFuture<>());
        Event event = new Event(MERCHANT_TRANSACTION_REQUESTED, new Object[]{new HashSet<MerchantTransaction>(), correlationId});
        queue.publish(event);
        return correlationsTransaction.get(correlationId).join();
    }

    public Set<MerchantTransaction> transactionReportGenerated(Event e) {
        Set<MerchantTransaction> t = (Set<MerchantTransaction>) e.getArguments()[0];
        var correlationId = e.getArgument(1, CorrelationId.class);
        correlationsTransaction.get(correlationId).complete(t);
        return t;
    }


    // *nicetohave* cancel requested transaction


    /* Query Operations */

}
