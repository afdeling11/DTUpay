package Merchant.aggregate;

import Merchant.aggregate.ID;
import Merchant.aggregate.Token;
import lombok.Setter;

@Setter
public class MerchantTransaction {

    private ID id;
    private Token token;
    private String merchantID;
    private int amount;
    private String description;


}
