package Merchant.repositories;

import Merchant.aggregate.ID;
import Merchant.aggregate.Merchant;
import messaging.MessageQueue;

public class MerchantRepository {
    private EventStore eventStore;

    public MerchantRepository(MessageQueue messageQueue) {
        this.eventStore = new EventStore(messageQueue);
    }

    public Merchant getMerchantById(ID id){
        return Merchant.createFromEvents(eventStore.getEventsFor(id));
    }

    public void save(Merchant merchant){
        eventStore.addEvents(merchant.getId(),merchant.getAppliedEvents());
        merchant.clearAppliedEvents();
    }

    public void remove(ID id){
        eventStore.removeEvents(id);
    }
}