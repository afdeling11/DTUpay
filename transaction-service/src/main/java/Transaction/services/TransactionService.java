package Transaction.services;

import Transaction.aggregate.*;
import lombok.Getter;
import Transaction.repositories.ReadTransactionModelRepository;
import Transaction.repositories.TransactionRepository;
import messaging.MessageQueue;
import messaging.Event;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class TransactionService {

    public static final String MERCHANT_TRANSACTION_INITIATED = "MerchantTransactionInitiated";
    public static final String MERCHANT_TRANSACTION_CREATED = "MerchantTransactionCreated";
    public static final String MERCHANT_TRANSACTIONS_REQUESTED = "CustomerTransactionsRequested";
    public static final String MERCHANT_TRANSACTION_REPORT_GENERATED = "MerchantTransactionReportGenerated";
    public static final String CUSTOMER_TRANSACTIONS_REQUESTED = "CustomerTransactionsRequested";
    public static final String CUSTOMER_TRANSACTIONS_REPORT_GENERATED = "CustomerTransactionsReportGenerated";

    @Getter
    private TransactionRepository repository;
    private ReadTransactionModelRepository readTransactionModelRepository;
    MessageQueue queue;

    public TransactionService(TransactionRepository repository, ReadTransactionModelRepository readTransactionModelRepository, MessageQueue q) {
        this.repository = repository;
        this.readTransactionModelRepository = readTransactionModelRepository;
        this.queue = q;
        this.queue.addHandler(MERCHANT_TRANSACTION_INITIATED, this::handleMerchantTransaction);
        this.queue.addHandler(CUSTOMER_TRANSACTIONS_REQUESTED, this::handleCustomerTransactionsRequested);
        this.queue.addHandler(MERCHANT_TRANSACTIONS_REQUESTED, this::handleMerchantTransactionsRequested);
    }

    /* Command operations */

    public Transaction createTransaction(ID id, Token token, String merchantId, int amount, String description){
        Transaction transaction = Transaction.create(id,token, merchantId, amount, description);
        repository.save(id, transaction);
        return transaction;
    }

    /* Query operations */
    public Set<Transaction> transactions(){
        return readTransactionModelRepository.getTransactions();
    }

    public Set<Transaction> transactionsById(ID id){
        return readTransactionModelRepository.getTransactionsById(id);
    }

    /* Handling stuff */
    public void handleMerchantTransaction(Event e){
        SubTransaction t = e.getArgument(0, SubTransaction.class); //(Transaction) e.getArguments()[0];
        CorrelationId correlationId =e.getArgument(1,CorrelationId.class);

        Transaction transaction = createTransaction(t.getId(),t.getToken(),t.getMerchantID(),t.getAmount(),t.getDescription());

        SubTransaction subTransaction = new SubTransaction();
        subTransaction.setMerchantID(transaction.getMerchantID());
        subTransaction.setAmount(transaction.getAmount());
        subTransaction.setDescription(transaction.getDescription());
        subTransaction.setToken(transaction.getToken());
        subTransaction.setId(transaction.getId());

        Event event = new Event(MERCHANT_TRANSACTION_CREATED, new Object[]{subTransaction,correlationId});
        queue.publish(event);
    }

    // Add a handler for CUSTOMER_TRANSACTION_REQUESTED
    public void handleCustomerTransactionsRequested(Event e) {
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);
        ID customerId = e.getArgument(2, ID.class);
        Set<Transaction> transactions =  transactionsById(customerId);
        Set<SubTransaction> subTransactions = new HashSet<>();
        for (Transaction t : transactions){
            SubTransaction temp = new SubTransaction();
            temp.setMerchantID(t.getMerchantID());
            temp.setAmount(t.getAmount());
            temp.setDescription(t.getDescription());
            temp.setToken(t.getToken());
            temp.setId(t.getId());
            subTransactions.add(temp);
        }

        Event event = new Event(CUSTOMER_TRANSACTIONS_REPORT_GENERATED, new Object[]{subTransactions, correlationId});
        queue.publish(event);
    }

    // Add a handler for MERCHANT_TRANSACTION_REPORT_GENERATED
    public void handleMerchantTransactionsRequested(Event e) {
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);
        ID merchantId = e.getArgument(2, ID.class);
        Set<Transaction> transactions =  transactionsById(merchantId);

        Set<SubTransaction> subTransactions = new HashSet<>();
        for (Transaction t : transactions){
            SubTransaction temp = new SubTransaction();
            temp.setMerchantID(t.getMerchantID());
            temp.setAmount(t.getAmount());
            temp.setDescription(t.getDescription());
            temp.setToken(t.getToken());
            temp.setId(t.getId());
            subTransactions.add(temp);
        }


        Event event = new Event(MERCHANT_TRANSACTION_REPORT_GENERATED, new Object[]{subTransactions, correlationId});
        queue.publish(event);
    }
    // Add a script that functions as a handler, and publishes a CUSTOMER_TRANSACTION_REPORT_GENERATED event
}
