package Transaction.repositories;

import Transaction.aggregate.ID;
import Transaction.aggregate.Transaction;
import messaging.MessageQueue;

public class TransactionRepository {
    private EventStore eventStore;

    public TransactionRepository(MessageQueue messageQueue) {
        this.eventStore = new EventStore(messageQueue);
    }

    public Transaction getTransactionById(ID id){
        return Transaction.createFromEvents(eventStore.getEventsFor(id));
    }

    public void save(ID id, Transaction transaction){
        eventStore.addEvents(id, transaction.getAppliedEvents());
        transaction.clearAppliedEvents();
    }

    public void remove(ID id){
        eventStore.removeEvents(id);
    }

}
